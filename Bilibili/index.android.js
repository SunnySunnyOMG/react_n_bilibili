/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Dimensions,
  Image
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons'

const {height, width}=Dimensions.get('window');
export default class Bilibili extends Component {
  render() {
    return (
      <View>
        <View style={{height:height*0.15, backgroundColor: '#FB7299', elevation:4}}>       
          <View style={{flex:1, flexDirection:'row'}}>

            <View style={{flex:1,flexDirection:'row', justifyContent:'flex-start',alignItems:'center'}}>
              <Icon name='md-menu' color="#FAFAFA" size = {30} style = {{paddingLeft:-15}}/>
              <Text style={{color:"#FAFAFA", fontSize:20 ,paddingLeft:10}}>Login</Text>

            </View>

            <View style={{flex:1,flexDirection:'row', justifyContent:'flex-end',alignItems:'center'}}>
              <Icon name='md-download' color="#FAFAFA" size = {30} style = {{paddingRight:15}}/>
              <Icon name='md-search' color="#FAFAFA" size = {30} style = {{paddingRight:15}}/>
            </View>

          </View>   
          <View style ={{ flex:1, flexDirection:'row',justifyContent:'space-around',alignItems:'flex-end',padding:10}}>
            <Text style = {{color:"#FAFAFA"}}>LIVE</Text>
            <Text style = {{color:"#FAFAFA"}}>HOME</Text>
            <Text style = {{color:"#FAFAFA"}}>VOID1</Text>
            <Text style = {{color:"#FAFAFA"}}>VOID2</Text>
  
          </View>  
        </View>

        <View style={{height:height*0.08, backgroundColor: '#F4F4F4',flexDirection:'row',alignItems:'center'}}>
          <View style={{flex:2, flexDirection:'row',justifyContent:'space-around',alignItems:'flex-start'}}>
            <Text>All</Text>
            <Text>|</Text>
            <Text>Uppers</Text>
          </View>
          <View style={{flex:1}}></View>
          <View style={{flex:2, alignItems:'flex-end'}}>
            <Text>23333</Text>
          </View>
        </View>
        <View style={{flex:1,flexDirection:'row',justifyContent:'space-around',flexWrap:'wrap', margin:10}}>
           <View style={{width: width*0.4, height: height*0.4, borderRadius:5, borderWidth:1, borderColor:"#FAFAFA",margin:5}}>
            <Image source={require('./leimu.png')} style ={{width: width*0.4, height:height*0.2}}>
              <Text style={{position:'absolute',bottom:2, left:width*0.02, fontSize:12, color:"#FAFAFA"}}>
                Play
              </Text>
              <Text style={{position:'absolute',bottom:2, right:width*0.02, fontSize:12, color:"#FAFAFA"}}>
                Bullet
              </Text>
            </Image>
           <View style={{height:height*0.2, backgroundColor: '#FAFAFA'}}>
             <Text style={{padding:10,paddingTop:5}}>titletitle</Text>
             <Text style={{padding:10,paddingTop:0}}>Tag</Text>
             <View style = {{flex:1, flexDirection:'row'}}>
              <Image source = {require('./sunny.png')} style={{marginLeft:10,width:width*0.1,height:width*0.1,borderRadius:width*0.1}}></Image>
              <View style={{flex:1, flexDirection:'column',paddingLeft:10}}>
               <Text>Username</Text>
               <Text>UploadTime</Text>
             </View>
             </View>
             
           </View>
           </View>
         
        </View>


      </View>
    );
  }
}

const styles = StyleSheet.create({

});

AppRegistry.registerComponent('Bilibili', () => Bilibili);
